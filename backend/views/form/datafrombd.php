<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Данные из БД';
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1>Данные из БД</h1>
    <ul>
        <?php foreach ($users as $user): ?>
            <li>
                <label>Имя</label>:<?= Html::encode("{$user->username}") ?><br>
                <label>Email</label>: <?= Html::encode($user->email) ?><br>
                <label>Телефон</label>: <?= Html::encode($user->phone) ?><br>
                <label>Время сохранения сообщения</label>: <?= Html::encode($user->time_sent) ?><br>
                <label>IP-адрес</label>: <?= Html::encode($user->ip_user) ?><br>
                <label>Сообщение</label>:<?= $user->message ?>
            </li>
        <?php endforeach; ?>
    </ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>