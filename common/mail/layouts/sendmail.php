<?php
use yii\helpers\Html;

$session = Yii::$app->session;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <p>Здравствуйте!</p>
    <p>Пользователь <?= $session['users']['username']  ?></p>
    <p>отправил Вам сообщение с почты <?= $session['users']['email'] ?>.</p>
    <p>Для связи оставил номер телефона: <?= $session['users']['phone'] ?>.</p>
    <p><?= $session['users']['message'] ?></p>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
