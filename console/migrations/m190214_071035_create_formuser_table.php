<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%formuser}}`.
 */
class m190214_071035_create_formuser_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%formuser}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(50)->notnull(),
            'email' => $this->string(50)->notnull()->unique(),
            'phone' => $this->string(20),
            'message' => $this->text(),
            'ip_user' => $this->string(15)->notnull(),
            'time_sent' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%formuser}}');
    }
}
