<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Form;

class FormController extends Controller
{
    public function actionCreate()
    {
        $model = new Form();
        if( $model->load(Yii::$app->request->post()) && $model->validate()  ) {
            $model->ip_user = Yii::$app->request->userIP;
            Yii::$app->session->setFlash('mailerFormSubmitted');
            if( $model->save() ) {
                $model->sendEmail();
                return $this->render('create-confirm', ['model' => $model]);
            }
        }
        return $this->render('create', ['model' => $model]);
    }
}