<?php
namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

class Form extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%formuser}}';
    }
    public function rules()
    {
        return [
            [['username', 'email','phone','message'], 'required'],
            ['email', 'email'],
            [['username'], 'string', 'max' => 50],
            [['phone'], 'match', 'pattern' => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/' ],
            [['message'], 'string', 'max' => 500],
        ];
    }
    public function attributeLabels()
    {
        return [
            'username' => 'Имя',
            'phone' => 'Телефон',
            'message' => 'Сообщение',
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['time_sent']],
                    'value' => function() { return date('Y-m-d H:i:s'); },

            ],
        ];
    }
    public function sendEmail()
    {
        Yii::$app->session['users'] = [
            'username' => $this->username,
            'email' => $this->email,
            'phone' => $this->phone,
            'message' => $this->message,
        ];
        if ($this->validate()) {
            Yii::$app->mailer->compose('sendmail')
                ->setTo(Yii::$app->params['adminEmail'])
                ->setFrom([$this->email => $this->username])
                ->setSubject('Данные с формы')
                ->send();

            return true;
        }
        return false;
    }
}