<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Введённые данные';
$this->params['breadcrumbs'][] = $this->title;

?>
<p>Вы ввели следующую информацию:</p>

<ul>
    <li><label>Имя</label>: <?= Html::encode($model->username) ?></li>
    <li><label>Email</label>: <?= Html::encode($model->email) ?></li>
    <li><label>Телефон</label>: <?= Html::encode($model->phone) ?></li>
    <li><label>Сообщение</label>: <?= Html::encode($model->message) ?></li>
    <li><label>Время сохранения сообщения</label>: <?= Html::encode($model->time_sent) ?></li>
    <label>Ввести ещё, нажмите на кнопку</label>: <a class="btn btn-success" href=" <?= Url::to('/create')?> ">Ещё</a>
</ul>