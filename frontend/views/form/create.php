<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Добавить данные';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'username')?>

<?= $form->field($model, 'email') ?>

<?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '+7 (999) 999 99 99',]) ?>

<?= $form->field($model, 'message') -> textarea()?>

    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end();
?>